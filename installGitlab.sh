###############################################################
# See https://about.gitlab.com/install/#ubuntu

usage: 

installGitlab.sh dnsName

################################################################

sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

# Install Post Fix
sudo apt-get install -y postfix

# Add the GitLab package repository and install the package
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL=$1 apt-get install gitlab-ee

##############################################################################
